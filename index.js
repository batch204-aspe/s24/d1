/*
	Exponent Operator

*/
// Using the Exponent Operator
const firstNum = 8 **3;
console.log("USing Exponent Operator of ES6 Updates 8**3 is:  " +firstNum);

// Before the ES6 Updates
const secondNum = Math.pow(8,3);
console.log("Before ES6 Updates of exponent Math.pow(8,3) is: "+secondNum);

/*
	Template Literals
		► Allows to write strings using the concatenation operator (+)

		► ${} - is a placeholder when using template literals


*/

let name = "John";

// Pre-Template Literal String
// Using single quote ('')
let message = 'Hello ' + name + ' ! Welcome to programming!';
console.log("Message without template literals: " + message);

// Strings using Template Literals
// Using backticks (``) for calling or concatenate the variable string ${variableName}
message = `Hello ${name}! Welcome to programming!`;
console.log(`Message with template literals: ${message}`);

// Multi-Line using Template Literals
const anotherMessage = `${name} attended a match competition.
He won it by solving the problem 8**3 with the solution of ${firstNum}.`
console.log(anotherMessage);

const interestRate = .1;
const principal = 1000;
console.log(`The intereset on your savings is: ${principal * interestRate}`);


// Array Destructuring
/*
	Array Destructuring
		► ALlows us to unpack elements in arrays into distinct variables

		► Allows us to name array elements with variables instead of using index numbers

	SYntax:
		let/const [variableName1, variableName2, variableName3] = array

*/

const fullName = ["Joe", "Dela", "Cruz"];

// Pre-Array Destructuring
console.log(fullName[0]);
console.log(fullName[1]);
console.log(fullName[2]);


// Array Destructuring
const [firstName, middleName, lastName] = fullName;
console.log(firstName);
console.log(middleName);
console.log(lastName);

console.log(`Hello, ${firstName} ${middleName} ${lastName}!`);


//Object Destucturing
/*
	Object Destucturing
		► Alows to unpack properties of objects into distinct variables

		► Shortens the syntax for accessing properties from objects

	SYNTAX:
		let/const { propertyName, propertyName } = object

*/

const person = {
	givenName: "Raf",
	maidenName: "Vien",
	familyName: "Santillan"
};

// Pre-Object Destructurin
console.log("Before Object Destructuring");
console.log(person.givenName);
console.log(person.maidenName);
console.log(person.familyName);

// Using Object Destruturing
console.log("Using the Object Destructuring");
const { givenName, maidenName, familyName } = person;
console.log(givenName);
console.log(maidenName);
console.log(familyName);


// Object Destructuring in functions
function getFullName({givenName, maidenName, familyName}){

	console.log(`${givenName} ${maidenName} ${familyName}`);

} getFullName(person);

// Arrow Functions
/*
	Arrow Functions
		► COmpact alternative syntax to traditional functions

	SYntax: 
		
		let/const variableName = () => {
			// codeblock/statement
			console.log();
		}

*/

// USing Arrow Function
const ello = () => {
	console.log("Hello World");
} 
ello();


function hello () {
	console.log("Hello World");
} hello();
// Pre-Arow Function & template Literals
/*

	Syntax:
		function functionName(parameterA, parameterB){
			statement/codeblock
		}

*/

function printFullName(firstName, middleInitial, lastName) {
	console.log(firstName + ' ' + middleInitial + '. ' + lastName);
}
printFullName("Michael", "P", "Calasin");

/*
	Mini Activity
		► Convert our function into Arrow Function

*/

// Arrow Function w/ parameter and arguments
const justPrintFN = (firstName, middleInitial, lastName) => {
	console.log(`${firstName} ${middleInitial}. ${lastName}`);
}
justPrintFN("Michael", "P", "Calasin");

// Arrow Function using Array Iteration Method
// Pre-Arrow Function
const students = ["Aron", "Daphne", "Edvic", "Angelo", "Roxanne"];

students.forEach(function(student){
	console.log(students + " is a student.");
});

// Arrow Function convert to anonymous function
students.forEach((student) => {
		console.log(`${student} is a student`)
	});

// Implicit return statement
// Pre-arrow Function
function add (x,y){
	return x + y;
}
let total = add (100, 200);
console.log(total);

// Arrow Function
const add1 = (x, y) => x + y;

let total1 = add (100, 200);
console.log(total1);


// if multiple line you nedd to explicity write the return
let add2 = (x, y) => {
	return x + y; 
}
let totalAdd = add2 (1, 1);
console.log(totalAdd);

// Default Function Argument Value
const greet = (name = 'User', age = 18) => {
	return `Good Morning, ${name} Age: ${age}`
}
console.log(greet()); // Good Morning, User Age: 18
console.log(greet("Mikki", 17)); // Good Morning, Mikki Age: 17

/*
	Class-Base Object Blueprint
		► Allow creation/instantiation of object using classes as blueprints

	SYntax:
		class className {
			constructor( objectPropertyA, objectPropertyB) {
				this.objectPropertyA = objectPropertyA;
				this.objectPropertyB = objectPropertyB;
			}
		}
*/

class Car {
	constructor (brand, name, year) {
		this.brand = brand;
		this.brand = name;
		this.year = year;
	}
}
/*
	function Car (brand, name, year) {
		this.brand = brand;
		this.brand = name;
		this.year = year;
	}
*/ 
const myCar = new Car();
console.log(myCar);

// Assigning the Value of property Object
myCar.brand = "Ford";
myCar.name = "ranger Raptor";
myCar.year = 2021;

console.log(myCar);

// or you can declare like this
const myNewCar = new Car ("Chevrolet", "Everest", 2021);
console.log(myNewCar);